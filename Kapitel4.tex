In diesem Kapitel erfolgt eine ausschnittsweise Spezifikation der abstrakten Syntax und Semantik der in Kapitel 4 vorgestellten Diagrammtypen. Die Designentscheidungen werden natürlichsprachlich erörtert und im Anschluss anhand von ausschnitthaften Meta-Modellen präsentiert. Dieser Abschnitt stellt gleichzeitig den vierten Schritt des Makro-Prozesses zur Erstellung einer DSML dar.

\section{Eignungsprüfung des Projektbegriffs}


Gemäß dem Vorschlag von Frank (2011) sollen die zuvor identifizierten zentralen Begriffe einer Eignungsprüfung unterzogen werden \parencite[S. 97-100]{Frankguidelines}. Dabei wird durch die Anwendung von Kriterien entschieden, ob ein Begriff Bestandteil der DSML werden sollte und ob er als Meta-Typ oder als Typ rekonstruiert wird. Die dabei herangezogenen Kriterien lauten wie folgt:

\textbf {a) Invariante Semantik:} Ist die Semantik des Begriffs innerhalb der Domäne und im Zeitverlauf invariant?

\textbf{b) Relevanz:} Findet der Begriff in der Domäne häufig genug Anwendung um als Sprachkonzept in die DSML ausgenommen zu werden?

\textbf{c) Semantische Varianz:} Weisen die instantiierten Typen, die aus dem Meta-Typ hervorgehen, eine hinreichende Varianz mit deutlichen semantischen Unterschieden auf?

\textbf{d) Instanz als Typ intuitiv:} Würden Sprachanwender die Instanzen des Begriffs intuitiv als Typ oder als Instanz betrachten?

Kriterium \textbf{a)} und \textbf{b)} werden angewendet, um zu entscheiden, ob ein Begriff überhaupt Bestandteil der DSML werden sollte, während durch Kriterium \textbf{c)} und \textbf{d)} entschieden wird, ob er als Meta-Typ in die Sprache aufgenommen wird. Sollte ein Begriff sich nicht als Meta-Typ qualifizieren, wird über Kriterium \textbf{e)} und \textbf{f)} entschieden, ob er als Typ repräsentiert wird.

\textbf{e) Instanz als Abstraktion:} Erlaubt ein Begriff keine weiteren sinnvollen Instanziierungen? 

\textbf{f) Invariante und einzigartige Instanzidentität:} Haben Begriffe bereits auf Typebene eine eindeutige Identität und sind die für den Modellierungszweck relevanten Merkmale während der Lebensdauer des Modells weitgehend unveränderlich?

Die zentralen Konzeptkandidaten, die hinsichtlich dieser Kriterien untersucht werden sollen, sind die aus den Diagrammtypen entnommenen Begriffe: \textit{Projekt, Organisationseinheit} und \textit{Prozess}. Exemplarisch wird an dieser Stelle die Überprüfung des Konzeptkandidaten \textit{Projekt} vorgenommen. (Abbildung \ref{fig:projektbegrifftypuntersuchung})

Die Analyseergebnisse für den Projektbegriff zeigen, dass dieser sich sowohl für die Aufnahme in die DSML, als auch als Meta-Typ Kandidat qualifiziert hat. Das Kriterium \textit{Instanz als Typ intuitiv} kann zwar als nicht vollständig erfüllt angesehen werden, jedoch wird davon ausgegangen, dass im Kontext des Projektmanagements bzw. Multiprojektmanagements, auf welche die DSML abzielt ein hinreichendes Verständnis vorliegt, um \textit{Projekt} als Typ zu identifizieren. Dennoch ist zu bemerken, dass diese Einschätzung subjektiv ist. Die Prüfung eines Begriffs hinsichtlich dieser Kriterien ist nicht immer möglich und liefert gegebenenfalls keine eindeutigen Ergebnisse. 

\begin{figure}
	\centering
	\includegraphics[width=12cm]{Eignungprojektbegriff.PNG}
	\caption{Kriterienprüfung des Projektbegriffs}
	\label{fig:projektbegrifftypuntersuchung}
\end{figure}
\section{Diskussion und Designentscheidungen zum Projektübersicht-Diagramm}

Das Kernkonzept dieses Diagrammtyps \textit{Projekt} kann, wie in Kapitel 2 ausgeführt, eine Vielzahl von Attributen bereitstellen, die es auszeichnen (Mitarbeiterzahl etc.). Jedoch sollen unter dem Aspekt der Zweckdienlichkeit nur solche Attribute Verwendung finden, die für die Auswahl von Projekten relevant erscheinen. Diese sind, wie schon in Kapitel 4.6.1 beschrieben, der Strategiewert (\textit{strategicValue}), Risikowert (\textit{riskValue}), Wirtschaftlichkeitswert (\textit{economicValue}) und ein Gesamtscore (\textit{aggregatedValue}), welcher sich aus den anderen Werten ergibt. Weiterhin soll ein Projekt über einen Namen (als eindeutiger Bezeichner) und eine Klassifizierungsmöglichkeit in Muss-Projekte (z.B. resultierend aus juristischen Zwängen) oder dringliche Projekte (z.B. resultierend aus hohem Kundendruck) verfügen\footnote{\textit{Diese Klassifizierung wird in der Literatur nicht vorgenommen. Jedoch erscheint es sinnvoll in einem Modell zur Projektauswahl unterschiedliche Dringlichkeitsstufen darstellen zu können um eine genauere Selektion zu ermöglichen.}}. Diese Klassifizierung wird durch den \textit{LocalImportanceType} vorgenommen. Der Risikowert wird, wie in Kapitel 4.2.3 angeführt, mit einem negativen Vorzeichen versehen. 

\subsection{Abstraktionsgrad der Projekte und Projektattribute}

Die Entscheidung über den Abstraktionsgrad der Projekte ist nicht leichtfertig zu treffen. Zum einen liegt die Überlegung nahe, bereits auf Typebene die Projekte mit einer möglichst aussagekräftigen Semantik anzureichern. Unter der Annahme, dass für die zu vergleichenden Projektarten (Softwareprojekte, FuE-Projekte) die gleichen Bewertungskriterien relevant sind, und diese keine Unterschiede aufweisen, können die Bewertungsattribute schon auf Typebene festgelegt werden. Dadurch wird jedoch der Wiederverwendungsnutzen geringer, falls eine Projektart unter anderen Bewertungskriterien vergleichbar gemacht werden soll. Auf Instanzebene wäre es nicht mehr möglich, spezifischere Sachverhalte oder Bewertungsattribute zu modellieren. Um dem Zweck der Projektbewertung dienlich zu sein sollte die Semantik der Instanzen jedoch möglichst einheitlich sein, was wiederum für eine Festlegung auf Typebene spricht. Dennoch werden Anwender am ehesten die Instanzebene adressieren, da spezifische Projekte miteinander verglichen werden. Die substantiellen Informationen (wie z.B. der konkrete\textit{ riskValue}), um eine Projektauswahl treffen zu können, sind demnach erst auf Instanzebene verfügbar.  Sollten die aus der Projektbewertung resultierenden Kennzahlen in ein Informationssystem eingepflegt worden sein, so ergibt sich die Möglichkeit die Werte aus diesem zu beziehen.  



\subsection{Interdependenzbeziehungen}
Wie die Ausführungen in Kapitel 4.2.4 ergeben haben lassen, sich Interdependenzbeziehungen zwischen den Projekten auf unterschiedliche Weise darstellen. Zur Erstellung des Meta-Modells müssen Überlegungen angestellt werden, wie sich diese rekonstruieren lassen.
Zunächst sollte von einer Wechselwirkung auf eine gerichtete Wirkung abstrahiert werden. Vor allem, da die Wirkungen, welche ein Projekt auf ein anderes Projekt hat, nicht zwingend auf das wirkende Projekt zurückgehen.\pagebreak

Bspw. muss ein Projekt, welches eine positive inhaltliche Wirkung auf ein anderes Projekt ausübt, nicht im selben Maße von diesem Projekt positiv beeinflusst werden.

Analog zu den Ausführungen im vorangegangenen Kapitel, können diese Wirkungen synergetisch oder konfliktär sein und entspringen aus Abhängigkeiten unterschiedlicher Natur. Hier erscheint es zweckgerichtet, nur auf solche Abhängigkeiten zu abstrahieren, welche einen Einfluss auf die Bewertungskriterien ausüben. Dementsprechend erscheint es nicht sinnvoll, diese in inhaltliche, zeitliche etc. Wirkung einzuteilen. Vielmehr liegt die Überlegung nahe, sie in eine strategische Abhängigkeit (\textit{strategicEffect}), Risiko Abhängigkeit (\textit{riskeffect}) und wirtschaftliche Abhängigkeit (\textit{economicEffect}) zu kategorisieren. Gegen diese Vorgehensweise kann argumentiert werden, dass diese Abhängigkeitsarten nicht in der vorliegenden Literatur identifiziert wurde. Der Einsatz einer Einflussmatrix erlaubt jedoch die Analyse bezüglich eben dieser Wirkungen oder Effekte, wodurch diese Herangehensweise implizit in der Literatur Verwendung findet. 

Kritisch zu beurteilen ist in diesem Zusammenhang jedoch, dass dafür eine einheitliche Normierung Voraussetzung ist. Die Wirkungen müssen zum einen in ihrer Wirkungsstärke einheitlich sein (z.B. ausgedrückt über einen Wertebereich von null bis zehn). Zum anderen müssen sich die entsprechenden Auswirkungen, auf die Bewertungsattribute des Zielprojekts, sinnvoll interpretieren lassen. Die dafür notwendigen Standardisierungen adressieren wiederum die identifizierte Herausforderung \textbf{H7} des Multiprojektmanagements.
 
Weiterhin sollten, entsprechend der Entscheidung, die Bewertungsattribute semantisch festzulegen, auch die Beziehungsattribute semantisch einheitlich sein. Die Begründung geht konform zu der oben genannten\footnote{\textit{Damit die Instanzen einheitlich und vergleichbar bleiben}}. Eine Wirkung kann synergetisch (\textit{positivInfluence}) oder konfliktär (\textit{negativeInfluence}) sein und über die geschilderten Effekte verfügen. Im Fall einer positiven Wirkung ist der entsprechende Effektwert, auf das zugehörige Bewertungsattribut des Zielprojektes hinzuzuaddieren, während er bei einer negativen Wirkung zu subtrahieren ist. 

\section{Diskussion über erste Designentscheidungen des Projekt-Ressourcenkonflikt-Diagramms}

Auch beim\textit{ Projekt-Ressourcenkonflikt-Diagramm} stellt der Begriff \textit{Projekt} das Kernkonzept dar. Zur Festlegung der Bearbeitungsrangfolge von Projekten sollen, Personalmittel (\textit{humanRessource}), resultierend aus den entsprechenden Personenbedarfsprofilen, und Finanzmittel (\textit{funds\footnote{\textit{Funds soll in diesem Fall kein wirtschaftliches Bewertungskriterum darstellen, sondern Angaben über Projektkosten enthalten.}}}) als Attribute der Projektentität dargestellt werden. Des Weiteren enthalten die Projekte ein Datum (\textit{startDate}), welches angibt wann das Projekt gestartet wird, um dieses horizontal entsprechend anordnen zu können. 

Zusätzlich soll die Modellierungssprache Ressourcenkonflikte zwischen Projekten abbilden können. Diese werden über den Relationstyp \textit{ressourceConflictRelation} dargestellt. Dabei lässt sich spezifizieren, ob Ressourcenkonflikte aus der Konkurrenz um Personalmittel (\textit{humanRelation}) oder Betriebsmitteln (\textit{inventoryRelation}) hervorgehen. 
Die Relation \textit{humanRelation} verfügt über das Attribut \textit{descripton}, das spezifiziert, um welche Personalmittel (z.B. in Form einer Mitarbeiterbeschreibung) konkurriert wird. Analog zu Kapitel 4.3.3 wird dazu über das Attribut \textit{manHour} angegeben, wie viele Personenstunden des jeweiligen Personals, in Konkurrenz stehen.

Die Relation \textit{inventoryRelation} soll ebenfalls über ein Attribut \textit{name} spezifizierbar sein und bspw. eine Maschine beschreiben, welche von zwei Projekten benötigt wird. Um dem Zweck der zeitlichen Abstimmung zwischen diesen Projekten dienlich zu sein wird zudem angegeben, von welchem Startzeitpunkt (\textit{utilizationStart}) und bis zu welchem Endzeitpunkt (\textit{utilizationEnd}) eine Beanspruchung der Ressource stattfindet. 

Beide betrachteten Relationstypen verfügen zudem über das Attribut \textit{postponable}, welches abgibt, ob eine zeitliche Verschiebung der Projekte (innerhalb einer betrachteten Periode), den Ressourcenkonflikt lösbar macht. Die Informationen über nötige Ressourcen, oder Ressourcenkonflikte können gegebenenfalls auch von Informationssystemen, wie einem ERP-System, bezogen werden. Entsprechend den Ausführungen zum \textit{Projektübersicht-Diagramm}, sind auch in diesem Fall die essenziellen Informationen, hinsichtlich benötigter Ressourcen und Ressourcenkonflikten, erst auf Instanzebene verfügbar, da konkrete Projekte miteinander verglichen werden.
Weiterhin sollen, zur Übersichtlichkeit und um Modelle nicht zu überladen, außer dem aggregatetValue, keine weiteren Bewertungsattribute (riskValue, economicValue, strategicValue) durch diesen Diagrammtyp dargestellt werden können\footnote{\textit{Diese Überlegungen müssten durch entsprechende Constraints dargestellt werden.}}. Diese Überlegung unterliegt der Annahme, dass nähergehende Bewertungsinformationen während der Festlegung der Bearbeitungsrangfolge nicht mehr notwendig sind, da die entsprechenden Projekte bereits ausgewählt wurden. 

\begin{figure}
	\centering
	\includegraphics[width=13cm]{MEMOLEGENDE}
	\caption{Ausschnitt der MEMO-MML nach (Frank 2011b, S.33)}
	\label{fig:memolegende}
\end{figure}
 

\subsection{Vorstellung des Meta-Modells}   
Aus den voran gegangenen Überlegungen lässt sich ein Meta-Modell (Abbildung \ref{fig:metamodell12}) ableiten, welches die Konzepte des \textit{Projektübersicht-} und \textit{Projekt-Ressourcenkonflikt-Diagramms} beinhaltet.\pagebreak 

Dargestellt wird dieses mit der MEMO Meta Modelling Language (MML). MEMO-MML verfügt über elaborierte Konzepte und eignet sich deshalb als Metamodellierungssprache für die Unternehmensmodellierung. Neben einer guten Dokumentation werden Konzepte für die Meta-Modellierung zur Verfügung gestellt, die es erlauben, unterschiedliche Abstraktionsebenen darzustellen. Weiterhin stellt MEMO-MML das Konzept der \textit{Intrinsic Features} zur Verfügung, welches die Möglichkeit bietet Attribute oder Eigenschaften zu Modellieren, welche erst auf Instanzebene offenbart werden \parencite[S. 104]{Frankguidelines}. Obwohl Meta-Modelle üblicherweise keine Typen beinhalten sollten \parencite{Frank2011memo}, erscheint es vor dem Hintergrund der vorgestellten Szenarien angemessen, da die essentiellen Informationen erst auf Instanzebene repräsentiert werden. Ein Ausschnitt zur Legende der MEMO-MML befindet sich in Abbildung \ref{fig:memolegende}.   





\begin{figure}
	\centering
	\includegraphics[width=13cm]{Meta-Modell12FINAL.PNG}
	\caption{Meta-Modell des Projekt-Ressourcenkonflikt und Projektübersicht-Diagramms}
	\label{fig:metamodell12}
\end{figure}





\subsection{Möglicher Umgang mit Programmen und Portfolios}

Die Ausführungen zu den Diagrammtypen haben ergeben, dass auf Typebene kaum essentielle Informationen verfügbar sind. Neben der Vergleichbarkeit von Projekten ist für das Multiprojektmanagement jedoch auch die Gegenüberstellung von Programmen und Portfolios denkbar. In diesem Fall müssen andere Überlegungen hinsichtlich des Abstraktionsgrads der Entitäten angestellt werden. Unter Berücksichtigung von bspw. Programmen (die Ausführungen lassen sich jedoch auch auf Portfolios anwenden) ergibt sich nämlich die Möglichkeit, bereits auf Typebene spezifische Informationen verfügbar zu machen. Die Kennzahlen zur Bewertung von Programmen entstammen typischerweise aus der Zusammenfassung von Bewertungsergebnissen, der in diesen Programmen gebündelten Projekten. Die konkreten Einzelprojektergebnisse lassen sich auf Instanzebene erfassen und auf Typebene zu einem aussagekräftigen Wert verdichten. Zur Auswahl von Programmen ist eine Berücksichtigung der Instanzebene nicht mehr notwendig. Die Attribute der einzelnen Projekte lassen sich in diesem Fall auf Instanzebene weiter spezifizieren. Über diesen Ansatz können die Abstraktionsebenen verwendet werden um unterschiedlichen Zwecken bezüglich der Auswahl von Projekten oder Portfolios zu dienen. Abbildung \ref{fig:intrinsicfeatures} verdeutlicht diese Überlegungen. 

\begin{figure}
	\centering
	\includegraphics[width=8cm]{ProgrammModell.PNG}
	\caption{Darstellung zum möglichen Umgang mit Programmen}
	\label{fig:intrinsicfeatures}
\end{figure}


\section{Diskussion über erste Designentscheidungen des Organisationseinheit-Funktion-Diagramms}

Auch Überlegungen zu möglichen Designentscheidungen des \textit{Organisationseinheit-Funktion-Diagramms} sollen an dieser Stelle angeführt erfolgen. Diese werden anschließen in einem separaten Meta-Modell vorgestellt.

Die Kernkonzepte des Organisationseinheit-Funktion-Diagramms stellen die Begriffe \textit{Organisationseinheit} (\textit{OrganisationalUnit}) und \textit{Prozess} (\textit{Process}) dar\footnote{\textit{Eine Überprüfung dieser Begriffe hinsichtlich der Eignungskriterien für das Meta-Modell soll nicht erfolgen, da es sich lediglich um erste Entwürfe von Designentscheidungen handelt.}}. Eine Organisationseinheit verfügt über einen Namen und soll hierarchisch in das Unternehmen eingeordnet werden können. Gleichzeitig soll deutlich werden, ob die jeweilige Organisationseinheit dem Multiprojektmanagement oder dem Projektmanagement zuzuordnen ist. Dafür empfiehlt es sich, dem Vorschlag von Frank (2011) entsprechend, einen lokalen Meta-Typen zu definieren \parencite[S. 102]{Frankguidelines}. Der Meta-Typ \textit{LocalManagementype} erlaubt es, durch das Attribut \textit{Managmentname} zu spezifizieren, ob die Organisationseinheit dem Multiprojektmanagement oder dem Projektmanagement zugehörig ist. Weiterhin kann über das Attribut \textit{Level} eine hierarchische Einordnung erfolgen (bspw. durch die Angabe eines Wertes, welcher die hierarchische Höhe widerspiegelt). 

Auch die Entität \textit{Process} soll sich, über den Meta-Typ \textit{LocalProcessType}, den o.g. Ausführungen entsprechend zuordnen lassen. Über die Relation \textit{composeOf} wird angegeben, ob ein Prozess aus weiteren Teilprozessen besteht. Weiterhin beinhalten diese Methoden (\textit{Method}), welche bei der Ausführung des jeweiligen Prozesses zur Anwendung kommen.
 
Jedoch besitzen die in Kapitel 4 vorgestellten Methoden nur wenige semantische Gemeinsamkeiten. Die Aufstellung einer \textit{BSC} oder das Anwenden der \textit{Kapitalwert-Methode} basieren auf unterschiedlichen Daten und haben andere Vorgehensweisen. Hier würde es sich empfehlen weitere Differenzierungen vorzunehmen, semantische Gemeinsamkeiten zu identifizieren und diese entsprechend zu kategorisieren.\pagebreak

Aufgrund der exemplarischen Darstellung soll in dieser Ausarbeitung eine Methode jedoch lediglich über einen Namen und eine entsprechende Beschreibung verfügen. 

Die Relation \textit{FunctionRelation} verknüpft die Prozesse mit den jeweiligen verantwortlichen Organisationseinheiten. Diese lässt sich den Ausführungen in Kapitel 4.4.3 entsprechend spezifizieren. Demnach kann eine Organisationseinheit als Ausführer (\textit{ExecuteRelation}), Entscheider (\textit{DecisionRelation}), Kontrolleur (\textit{ControllRelation}) oder Informationssammler und Bereitsteller (\textit{CollectRelation}) mit einem Prozess verbunden sein. 

In diesem Fall sind die notwendigen Informationen, um ermitteln zu können welche Organisationseinheit an welchem Prozess Beteiligung hat, bereits auf Typebene verfügbar. Diese Überlegungen lassen sich, durch das in Abbildung \ref{fig:metamodelldiagramm3} dargestellte Meta-Modell, visualisieren.


\begin{figure}[H]
	\centering
	\includegraphics[width=14cm]{MetaModelldiagramm3}
	\caption{Meta-Modell des Organisationseinheit-Funktion-Diagramms}
	\label{fig:metamodelldiagramm3}
\end{figure}\pagebreak

\section{Möglicher Bezug zu anderen Modellierungssprachen}

Die betrachteten Diagrammtypen beschränken sich zu einem großen Teil, auf die Darstellung von Angaben über spezifische Bewertungskennzahlen oder notwendiger Ressourcen. Um eine detailliertere Ausgestaltung von z.B. bestimmten Ressourcengruppen zu ermöglichen, können jedoch auch Konzepte aus anderen Ansätzen der Unternehmensmodellierung Anwendung finden. So kann beispielsweise das \textit{Konzeptmodell} von 4EM (vgl. Kapitel 4.7.2) genutzt werden, um die Relationen \textit{humanRelation} oder \textit{inventoryRelation} des Projekt-Ressourcenkonflikt-Diagramms weiter zu spezifizieren. 

Beispielsweise beinhaltet die Ressourcenkonflikt Relation \textit{humanRelation}, eine Beschreibung der Personenmittel und eine Angabe über die notwendige voraussichtliche Arbeitszeit.  Das \textit{Konzeptmodell} ermöglicht es, diesen Angaben weitere Attribute hinzuzufügen und bestimmte Merkmale, welche z.B ein Mitarbeiter kennzeichnet, zu beschreiben. Denkbar sind in diesem Kontext Charakteristiken, wie eine bestimmte Qualifikation oder eine exaktere Beschreibung der Tätigkeit.

Diese Überlegungen lassen sich auch für die Ausgestaltung weiterer Konzepte, wie der \textit{OrganisationalUnit} oder dem \textit{Process} anwenden. 




%%Frank (2011) empfehlt zur Dokumentation der Begriffe das Anlegen eines Konzeptwörterbuchs welches für Das Projektübersicht-Diagramm in Abbildung \ref{fig:konzeptwoerterbuchprojektuebersicht} einzusehen ist. 








